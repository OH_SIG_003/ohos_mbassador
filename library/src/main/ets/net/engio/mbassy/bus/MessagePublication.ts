/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { IMessagePublication } from './IMessagePublication'
import { Subscription } from '../subscription/Subscription'
import { BusRuntime } from './BusRuntime'
import ArrayList from '@ohos.util.ArrayList';

export class MessagePublication implements IMessagePublication {
    private message: Object;
    private subscription: ArrayList<Subscription>;
    private runtime: BusRuntime;
    private state: State = State.Initial;

    constructor(runtime: BusRuntime, subscription: ArrayList<Subscription>, message: Object, state: State) {
        this.subscription = subscription;
        this.message = message;
        this.runtime = runtime;
        this.state = state;
    }

    execute() {
        this.state = State.Running;
        this.subscription.forEach((value, index) => {
            value.publish(this.message);
        })
        this.state = State.Finished;
    }

    executeAsyn() {
        this.state = State.Running;
        this.subscription.forEach((value, index) => {
            new Promise<string>((resolve, reject) => {
                try {
                    value.publish((this.message));
                    resolve("success");
                } catch (error) {
                    reject(error);
                }
            }).then((v) => {
                if (index == this.subscription.length - 1) {
                    this.state = State.Finished;
                }
            }).catch((err) => {
                console.log(err);
            })
        })

    }

    isFinished(): boolean {
        return this.state.valueOf() == State.Finished.valueOf() || this.state.valueOf() == State.Initial.valueOf();
    }

    isRunning(): boolean {
        return this.state.valueOf() == State.Running.valueOf();
    }

    isScheduled(): boolean {
        return this.state.valueOf() == State.Scheduled.valueOf();
    }

    hasError(): boolean {
        return false;
    }

    isDeadMessage(): boolean {
        return false;
    }

    isFilteredMessage(): boolean {
        return false;
    }

    getMessage(): Object {
        return this.message;
    }
}

export enum State {
    Initial, Scheduled, Running, Finished
}

export class MessagePublicationFactory {
    public createPublication(runtime: BusRuntime, subscriptions: ArrayList<Subscription>, message: Object): MessagePublication {
        return new MessagePublication(runtime, subscriptions, message, State.Initial);
    }
}